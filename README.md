# drb-topic-safe
The DRB plugin `drb-topic-safe` declare topics about *Standard Archive Format 
for Europe* ([**SAFE**](https://sentinels.copernicus.eu/web/sentinel/user-guides/sentinel-1-sar/data-formats/safe-specification))
format specification.


## Installation
```
pip install drb-topic-safe
```

## Topics
This section references topics defined in `drb-topic-safe`.

```mermaid
graph TB
    A([SAFE Product<br/>487b0c70-6199-46de-9e41-4914520e25d9])
    B([SAFE Manifest<br/>3289b9eb-4c8e-4d3b-8e37-8eeb843941f5])
```

