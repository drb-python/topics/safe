from drb.core.node import DrbNode
from drb.core.factory import DrbFactory


class DummyFactory(DrbFactory):
    def _create(self, node: DrbNode) -> DrbNode:
        return node
